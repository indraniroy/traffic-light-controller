We used online simulator to test our assertions. To use online simulator follow below steps:

1) go to link : https://www.edaplayground.com/?pageType=REGISTER
2) paste code of testbench and the design in corresponding part
3) To run use Synopsys VCS 2014.10 Simulator for execution
4) Run

The log will be displayed at the bottom. If there is any error, it will be displayed in the log with error message used in assertions. Else it will give the result. 
