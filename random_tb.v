module tb_traffic;
reg clock,reset;
wire[1:0] dir_ns,dir_sn,dir_ew,dir_we;

wire[3:0] car_ns, car_ew, car_sn, car_we;

//wire[3:0] car_ns,car_sn,car_ew,car_we;
controller c (.clock(clock), .reset(reset) , .dir_ns(dir_ns), .dir_sn(dir_sn), .dir_ew(dir_ew), .car_sn(car_sn),.car_ew(car_ew), .car_ns(car_ns), .car_we(car_we), .dir_we(dir_we));
//random r (clock,reset,car_ns,car_sn,car_ew,car_we);

initial
begin
	reset=0;
	clock=0;
	
	 $dumpfile("traffic.vcd");
    $dumpvars(0,tb_traffic);
	//$display("time   reset  car_ns   car_sn   car_ew   car_we ");
	$display("time   reset  dir_ns	car_ns  dir_ew  car_ew  dir_sn	car_sn  dir_we 	car_we	 ");
	$monitor("%3d	%b	%b	%d	%b	%d   %b  %d  %b  %d",$time,reset,dir_ns,car_ns,dir_ew,car_ew,dir_sn,car_sn,dir_we, car_we );


//$display("time   reset  car_ns   car_sn   car_ew   car_we "); 

	//$monitor("%3d	%b	%b	%b	%b	%b",$time,reset,car_ns,car_sn,car_ew,car_we);
	
	#20 reset = 1;
	#10 reset = 0;
	
	#100 $finish(0);

end

always
begin
    #1 clock=~clock;
end
endmodule
