
/* 
Project: 4-Way Traffic Light Controller
Purpose: Designing a traffic light controller that controls 4 traffic lights at a 4-point crossing
Created by: Indrani (CS16S20), Ramya (CS12B051), Sandeep (EE16M004)
Last modified: Feb 19, 2017
*/




/*
Module: car
Input: clock, reset, dir_ns, dir_ew, dir_we, dir_sn, car_ns_new, car_ew_new, car_sn_new, car_we_new
Output: car_ns, car_ew, car_sn, car_we
Purpose: Simulates cars on a road; at each clock cycle non-deterministically adds to cars on each road
*/

module car(
clock,
reset,
car_ns,
car_sn,
car_ew,
car_we,car_ns_new,car_ew_new,car_sn_new,car_we_new, dir_ns, dir_ew, dir_we,dir_sn    
);

	input clock,reset;
	input wire[1:0] dir_ns, dir_ew, dir_we,dir_sn;
	input[3:0] car_ns_new,car_ew_new,car_sn_new,car_we_new;
	wire[3:0] car_ns_new,car_ew_new,car_sn_new,car_we_new;
	
	output[3:0] car_ns,car_sn,car_ew,car_we; 
	reg[3:0] car_ns;
    reg[3:0] car_sn;
	reg[3:0] car_ew;
	reg[3:0] car_we;
	
	integer    seed,i,j;
	
   	initial car_ns=4'b0001;
   	initial car_sn=4'b0000;
	initial car_ew=4'b0000;
	initial car_we=4'b0000;
		
	always @(posedge clock)
	begin	
		if(dir_ns==2'b11)
		begin
			car_ns = car_ns_new+$urandom%2;
		end
		else if(dir_ew==2'b11)
		begin
			car_ew = car_ew_new+$urandom%2;
		end
		else if(dir_sn==2'b11)
		begin
			car_sn = car_sn_new+$urandom%2;
		end
		else if(dir_we==2'b11)
		begin
			car_we = car_we_new+$urandom%2;
		end
		else
		begin
			car_ns = car_ns+$urandom%2;
			car_ew = car_ew+$urandom%2;
			car_sn = car_sn+$urandom%2;
			car_we = car_we+$urandom%2;
		end
	end
endmodule


/*
Module: sensor
Input: clock, reset, car_ns, car_ew, car_sn, car_we, dir
Ouput: sensor
Purpose: Depending on the road (direction) under consideration outputs whether the traffic is nil (2'b01), light (2'b10) - less than 5 cars, or heavy(2'b11) - greater than equal to 5 cars
*/

module sensor(
clock,
reset,
car_ns,car_sn,car_ew,car_we,
dir,
sensor 
);

input clock,reset;
wire  clock,reset; 
input[3:0] car_ns,car_sn,car_ew,car_we;
wire[3:0]  car_ns,car_sn,car_ew,car_we;
input[1:0] dir;
wire[1:0] dir;

output[1:0] sensor;
reg[1:0] sensor;

initial sensor=2'b00;

always @ (posedge clock)
begin
	if(dir==2'b00)
		begin
			if(  car_ns==0)
				sensor=2'b01;
			else if(  car_ns<5 )
				sensor=2'b10;
			else if( car_ns >=5)
				sensor=2'b11;
	end 
	else if(dir==2'b01)
		begin
			if(  car_ew==0)
				sensor=2'b01;
			else if(  car_ew<5 ) begin
				sensor=2'b10;
			
				end
			else if( car_ew >=5) begin
				sensor=2'b11;
				end
		end 
	else if(dir==2'b10)
		begin
			if(  car_sn==0)
				sensor=2'b01;
			else if(  car_sn< 4'b0101 )
				sensor=2'b10;
			else if( car_sn >=5)
				sensor=2'b11;
		end 
	else if(dir==2'b11)
		begin
			if(  car_we==0)
				sensor=2'b01;
			else if(  car_we<5 )
				sensor=2'b10;
			else if( car_we >=5)
				sensor=2'b11;
		end 
end
endmodule


/*
Module: direction
Input: clock, reset, car_ns, car_ew, car_sn, car_we, sensor
Output: car_ns_new, car_ew_new, car_sn_new, car_we_new, dir_ns, dir_ew, dir_sn, dir_we, dir
Purpose: 
*/
module direction
(
clock,
reset,
sensor,
car_ns,
car_sn,
car_ew,
car_we,
dir_ns,dir_sn,dir_ew,dir_we,
dir,
car_ns_new, car_ew_new, car_sn_new, car_we_new
);


input   clock,reset;
input[3:0] car_ns,car_sn,car_ew,car_we ;

output[3:0]	car_ns_new,car_sn_new,car_ew_new,car_we_new;
reg[3:0] car_ns_new,car_sn_new,car_ew_new,car_we_new;

input[1:0] sensor;
output[1:0] dir_ns,dir_sn,dir_ew,dir_we;
output[1:0] dir;
wire   clock,reset;
wire[3:0]  car_ns,car_sn,car_ew,car_we ;
reg[1:0] dir_ns,dir_sn,dir_ew,dir_we;
reg[3:0] count;
reg[3:0] car;
reg[1:0] dir;
integer i,j;


parameter   NS= 4'b0001, EW = 4'b0010, SN = 4'b0100, WE=4'b1000 ;
reg   [3:0]          state        ;
reg   [3:0]          next_state ;
wire[1:0]  sensor;
initial dir_ns=2'b11;
initial dir_sn=2'b01;
initial dir_ew=2'b01;
initial dir_we=2'b01;
initial state = NS;
initial dir=2'b00;

initial car_ns_new = car_ns;
initial car_ew_new=car_ew;
initial car_sn_new=car_sn;
initial car_we_new=car_we;

always @ (posedge clock, posedge reset)
begin 

if (reset == 1'b1) begin
  state = NS;
end
else begin
 
 case(state)
   NS : 
   
   if (sensor == 2'b01) begin
                state = EW;
              end else if (sensor == 2'b10) begin
              
		dir_ns=2'b11;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b01;
		car=car_ns;
		car_ns_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_ns_new = car_ns_new - 1;
		end
		count=4-car;
		dir_ns=2'b10;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b01;
		#count;
		state= EW;
		dir=2'b01;
              end else if (sensor == 2'b11) begin
		dir_ns=2'b11;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b01;
		car=car_ns;
		if(car <=10)
		begin
		
		car_ns_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_ns_new = car_ns_new - 1;
		end
			count=10-car;
			dir_ns=2'b10;
			dir_sn=2'b01;
			dir_ew=2'b01;
			dir_we=2'b01;
			#count;
		end
			
		else if(car>10)
		begin
			for(i=0; i < 10; i=i+1)
			begin
				#1;
				car_ns_new = car_ns_new - 1;
			end
		end
		state = EW;
		
		dir=2'b01;
        end 
		
   EW : if (sensor == 2'b01) begin
                state = SN;
              end else if (sensor == 2'b10) begin
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b11;
		dir_we=2'b01;
		car=car_ew;
		car_ew_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_ew_new = car_ew_new - 1;
		end
	
		count=4-car;
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b10;
		dir_we=2'b01;
		#count;
		
		state= SN;
		
		dir=2'b10;
              end else if (sensor == 2'b11) begin
        
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b11;
		dir_we=2'b01;
		car=car_ew;
		
		if(car<=10)
		begin
		car_ew_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_ew_new = car_ew_new - 1;
		end
			count=10-car;
			dir_ns=2'b01;
			dir_sn=2'b01;
			dir_ew=2'b10;
			dir_we=2'b01;
			#count;
		end
		
		else if(car>10)
		begin
			for(i = 0; i < 10; i=i+1)
			begin
				#1;
				car_ew_new = car_ew_new-1;
			end
		end
		state = SN;
		
		dir=2'b10;
              end 
   SN : if (sensor == 2'b01) begin
                state = WE;
              end else if (sensor == 2'b10) begin
		dir_ns=2'b01;
		dir_sn=2'b11;
		dir_ew=2'b01;
		dir_we=2'b01;
		car=car_sn;
		
		car_sn_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_sn_new = car_sn_new - 1;
		end
		count=4-car;
		dir_ns=2'b01;
		dir_sn=2'b10;
		dir_ew=2'b01;
		dir_we=2'b01;
		#count;
		state= WE;
		
		dir=2'b11;
              end else if (sensor == 2'b11) begin
		dir_ns=2'b01;
		dir_sn=2'b11;
		dir_ew=2'b01;
		dir_we=2'b01;
		car=car_sn;
		
		if(car <= 10)
		begin
		car_sn_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_sn_new = car_sn_new - 1;
		end
			count=10-car;
			dir_ns=2'b01;
			dir_sn=2'b10;
			dir_ew=2'b01;
			dir_we=2'b01;
			#count;
		end
		
		else if(car>10)
		begin
			for(i = 0; i < 10; i = i+1)
			begin
				#1;
				car_sn_new = car_sn_new - 1;
			end
		end
		state = WE;
		
		dir=2'b11;
              end 
  WE : if (sensor == 2'b01) begin
                state = NS;
              end else if (sensor == 2'b10) begin
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b11;
		car=car_we;
		
		car_we_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_we_new = car_we_new - 1;
		end
		count=4-car;
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b10;
		#count;
		state= NS;
		
		dir=2'b00;
              end else if (sensor == 2'b11) begin
		dir_ns=2'b01;
		dir_sn=2'b01;
		dir_ew=2'b01;
		dir_we=2'b11;
		car=car_we;
		
		if(car <= 10)
		begin
		car_we_new = car;
		for(i=0;i<=car-1;i=i+1)
		begin
			#1;
			car_we_new = car_we_new - 1;
		end
			count=10-car;
			dir_ns=2'b01;
			dir_sn=2'b01;
			dir_ew=2'b01;
			dir_we=2'b10;
			#count;
		end
		
		
		else if(car>10)
		begin
			for(i = 0; i <10; i=i+1)
			begin
				#1;
				car_we_new = car_we_new-1;
			end
		end
		state = NS;
		
		dir=2'b00;
              end 
	 endcase
end
end

endmodule


module controller
(
clock,
reset,
dir_ns,dir_sn,dir_ew,dir_we ,car_ns,car_sn,car_ew,car_we
);

input clock,reset;
output[1:0] dir_ns,dir_sn,dir_ew,dir_we;
output[3:0] car_ns,car_sn,car_ew,car_we;

wire[3:0] car_ns,car_sn,car_ew,car_we ;
wire[3:0] car_ns_new,car_sn_new,car_ew_new,car_we_new ;
wire[1:0] sensor;
wire[1:0] dir;

car r(clock,reset,car_ns,car_sn,car_ew,car_we,car_ns_new,car_ew_new,car_sn_new,car_we_new,dir_ns, dir_ew, dir_we,dir_sn);
sensor s(clock,reset,car_ns,car_sn,car_ew,car_we,dir,sensor );
direction d(clock,reset,sensor,car_ns,car_sn,car_ew,car_we,dir_ns,dir_sn,dir_ew,dir_we,dir,car_ns_new,car_ew_new,car_sn_new,car_we_new);

endmodule

