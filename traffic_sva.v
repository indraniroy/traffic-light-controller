// Code your design here

/* 
Project: 4-Way Traffic Light Controller
Purpose: Designing a traffic light controller that controls 4 traffic lights at a 4-point crossing
Created by: Indrani (CS16S20), Ramya (CS12B051)
Last modified: Mar 19, 2017
*/


/*
Module: car
Input: clock, reset, dir_ns, dir_ew, dir_we, dir_sn, car_ns_new, car_ew_new, car_sn_new, car_we_new
Output: car_ns, car_ew, car_sn, car_we
Purpose: Simulates cars on a road; at each clock cycle non-deterministically adds to cars on each road
*/

module car(
clock,
reset,
car_ns,
car_sn,
car_ew,
car_we,car_ns_new,car_ew_new,car_sn_new,car_we_new, dir_ns, dir_ew, dir_we,dir_sn    
);

	input clock,reset;
	input wire[1:0] dir_ns, dir_ew, dir_we,dir_sn;
	input[3:0] car_ns_new,car_ew_new,car_sn_new,car_we_new;
	wire[3:0] car_ns_new,car_ew_new,car_sn_new,car_we_new;
	
	output[3:0] car_ns,car_sn,car_ew,car_we; 
	reg[3:0] car_ns;
    reg[3:0] car_sn;
	reg[3:0] car_ew;
	reg[3:0] car_we;
	
	integer    seed,i,j;
	
   	initial car_ns=4'b0001;
   	initial car_sn=4'b0000;
	initial car_ew=4'b0000;
	initial car_we=4'b0000;
		
	always @(posedge clock)
	begin	
		if(dir_ns==2'b11)
		begin
			car_ns = car_ns_new+$urandom%2;
		end
		else if(dir_ew==2'b11)
		begin
			car_ew = car_ew_new+$urandom%2;
		end
		else if(dir_sn==2'b11)
		begin
			car_sn = car_sn_new+$urandom%2;
		end
		else if(dir_we==2'b11)
		begin
			car_we = car_we_new+$urandom%2;
		end
		else
		begin
			car_ns = car_ns+$urandom%2;
			car_ew = car_ew+$urandom%2;
			car_sn = car_sn+$urandom%2;
			car_we = car_we+$urandom%2;
		end
	end
endmodule


/*
Module: sensor
Input: clock, reset, car_ns, car_ew, car_sn, car_we, dir
Ouput: sensor
Purpose: Depending on the road (direction) under consideration outputs whether the traffic is nil (2'b01), light (2'b10) - less than 5 cars, or heavy(2'b11) - greater than equal to 5 cars
*/

module sensor(
clock,
reset,
car_ns,car_sn,car_ew,car_we,
dir,
sensor 
);

input clock,reset;
wire  clock,reset; 
input[3:0] car_ns,car_sn,car_ew,car_we;
wire[3:0]  car_ns,car_sn,car_ew,car_we;
input[1:0] dir;
wire[1:0] dir;

output[1:0] sensor;
reg[1:0] sensor;
reg[1:0] old_dir;
initial old_dir=2'b11;



always @ (posedge clock)
begin
if (reset == 1'b1) 
begin
		  old_dir=2'b11;
end
	if(old_dir!=dir) 
	begin
	old_dir=dir;
	if(dir==2'b00)
		begin
			if(  car_ns==0)
				sensor=2'b01;
			else if(  car_ns<5 )
              	begin
				sensor=2'b10;
				#4;
                end
			else if( car_ns >=5)
				begin sensor=2'b11;
				#10;
                end
	end 
	else if(dir==2'b01)
		begin
			if(  car_ew==0)
				sensor=2'b01;
			else if(  car_ew<5 ) begin
				sensor=2'b10;	
				#4;		
				end
			else if( car_ew >=5) begin
				sensor=2'b11;
				#10;
		end
				
		end 
	else if(dir==2'b10)
		begin
			if(  car_sn==0)
				sensor=2'b01;
			else if(  car_sn< 4'b0101 )
				begin sensor=2'b10;
				#4;
                end
			else if( car_sn >=5)
				begin sensor=2'b11;
				#10;
                end
		end 
	else if(dir==2'b11)
		begin
			if(  car_we==0)
				sensor=2'b01;
			else if(  car_we<5 )
				begin sensor=2'b10;
				#4;
                end
			else if( car_we >=5)
				begin sensor=2'b11;
				#10;
                end
		end 
	end
end
  
  



endmodule


/*
Module: direction
Input: clock, reset, car_ns, car_ew, car_sn, car_we, sensor
Output: car_ns_new, car_ew_new, car_sn_new, car_we_new, dir_ns, dir_ew, dir_sn, dir_we, dir
Purpose: Main module that controls the signals at each crossing; decreaments cars as they cross the green light
*/
module direction
(
clock,
reset,
sensor,
car_ns,
car_sn,
car_ew,
car_we,
dir_ns,dir_sn,dir_ew,dir_we,
dir,
car_ns_new, car_ew_new, car_sn_new, car_we_new
);

input   clock,reset;
wire   clock,reset;
input[3:0] car_ns,car_sn,car_ew,car_we ;
wire[3:0]  car_ns,car_sn,car_ew,car_we ;
input[1:0] sensor;
wire[1:0]  sensor;

output[3:0]	car_ns_new,car_sn_new,car_ew_new,car_we_new;
reg[3:0] car_ns_new,car_sn_new,car_ew_new,car_we_new;
output[1:0] dir_ns,dir_sn,dir_ew,dir_we;
reg[1:0] dir_ns,dir_sn,dir_ew,dir_we;
output[1:0] dir;
reg[1:0] dir;

reg[3:0] count;
reg[3:0] car;
integer i,j;

parameter   NS= 4'b0001, EW = 4'b0010, SN = 4'b0100, WE=4'b1000 ;
reg   [3:0]          state        ;
reg   [3:0]          next_state ;

initial dir_ns=2'b11;
initial dir_sn=2'b01;
initial dir_ew=2'b01;
initial dir_we=2'b01;
initial state = NS;
initial dir=2'b00;
initial car_ns_new = car_ns;
initial car_ew_new=car_ew;
initial car_sn_new=car_sn;
initial car_we_new=car_we;


//-------------------------------------System Verilog Properties---------------------------


/*If direction North-South is active no other direction should active. (dir_xx=11 -> Green ,dir_xx=10 -> Yellow, dir_xx=01 -> Red)*/
property NorthSouthSignal; 
@(posedge clock)  
(dir_ns==2'b11 || dir_ns==2'b10) |-> (dir_sn==2'b01 && dir_ew==2'b01 && dir_we==2'b01);
endproperty

/*If direction East-West is active no other direction should active. (dir_xx=11 -> Green, dir_xx=10 -> Yellow, dir_xx=01 -> Red)*/

property EastWestSignal;
@(posedge clock)  
(dir_ew==2'b11 || dir_ew==2'b10) |-> (dir_sn==2'b01 && dir_ns==2'b01 && dir_we==2'b01);
endproperty

/*If direction South-North is active no other direction should active. (dir_xx=11 -> Green, dir_xx=10 -> Yellow, dir_xx=01 -> Red)*/

property SouthNorthSignal;
@(posedge clock)  
(dir_sn==2'b11 || dir_sn==2'b10) |-> (dir_ew==2'b01 && dir_ns==2'b01 && dir_we==2'b01);
endproperty


/*If direction West-East is active no other direction should active. (dir_xx=11 -> Green, dir_xx=10 -> Yellow, dir_xx=01 -> Red)*/
property WestEastSignal;
@(posedge clock)  
(dir_we==2'b11 || dir_we==2'b10) |-> (dir_sn==2'b01 && dir_ns==2'b01 && dir_ew==2'b01);
endproperty

//---------------------------------------

/*If state==NS then dir should give value 00. This is to check if state and dir variable are in sync or not*/

property StateActiveNS;
@(posedge clock)  
state==NS |-> dir==2'b00;
endproperty


/*If state==EW then dir should give value 00. This is to check if state and dir variable are in sync or not*/

property StateActiveEW;
@(posedge clock)  
state==EW |-> dir==2'b01;
endproperty


/*If state==SN then dir should give value 00. This is to check if state and dir variable are in sync or not*/
property StateActiveSN;
@(posedge clock)  
state==SN |-> dir==2'b10;
endproperty

/*If state==WE then dir should give value 00. This is to check if state and dir variable are in sync or not*/

property StateActiveWE;
@(posedge clock)  
state==WE |-> dir==2'b11;
endproperty



//-----------------------------------------


//If sensor==01 and direction NS is active it means there is no car in NS direction. So direction will change to EW.
property ZeroCarDelayNS;
@(posedge clock)  
  (state==NS && sensor == 2'b01) |->  state==EW;
endproperty


//if sensor==01 and direction EW is active it means there is no car in EW direction. So direction will change to SN.
property ZeroCarDelayEW;
@(posedge clock)  
(state==EW && sensor == 2'b01) |->  state==SN;
endproperty


//if sensor==01 and direction SN is active it means there is no car in SN direction. So direction will change to WE.
property ZeroCarDelaySN;
@(posedge clock)  
(state==SN && sensor == 2'b01) |->  state==WE;
endproperty


//if sensor==01 and direction WE is active it means there is no car in WE direction. So direction will change to NS.
property ZeroCarDelayWE;
@(posedge clock)  
(state==WE && sensor == 2'b01) |->  state==NS;
endproperty


//-----------------------------------------------------


//if sensor==10 and direction NS is active it means there is light load of cars in NS direction. Unless there is 0 cars in all other directions the direction will not be =NS after 5 clock tick. 
property LightCarDelayNS;
@(posedge clock)  

(state==NS && sensor == 2'b10 && car_ew!=0 && car_sn!=0 && car_we!=0 && !reset) |-> ##3 state!=NS;
endproperty


//if sensor==10 and direction EW is active it means there is light load of cars in EW direction. Unless there is 0 cars in all other directions the direction will not be =EW after 5 clock tick. 
property LightCarDelayEW;
@(posedge clock)  
(state==EW && sensor == 2'b10 && car_ns!=0 && car_sn!=0 && car_we!=0) |-> ##3 state!=EW;
endproperty


//if sensor==10 and direction SN is active it means there is light load of cars in SN direction. Unless there is 0 cars in all other directions the direction will not be =SN after 5 clock tick.
property LightCarDelaySN;
@(posedge clock)  
(state==SN && sensor == 2'b10 && car_ew!=0 && car_ns!=0 && car_we!=0) |-> ##3 state!=SN;
endproperty


//if sensor==10 and direction WE is active it means there is light load of cars in WE direction. Unless there is 0 cars in all other directions the direction will not be =WE after 5 clock tick. 
property LightCarDelayWE;
@(posedge clock)  
(state==WE && sensor == 2'b10 && car_ew!=0 && car_sn!=0 && car_ns!=0) |-> ##3 state!=WE;
endproperty


//--------------------------------------


//if sensor==11 and direction NS is active it means there is heavy load of cars in NS direction. Unless there is 0 cars in all other directions the direction will not be =NS after 10 clock tick. 
property HeavyCarDelayNS;
@(posedge clock)  
(state==NS && sensor == 2'b11 && car_ew!=0 && car_sn!=0 && car_we!=0 && !reset) |-> ##5 state!=NS;
endproperty


//if sensor==11 and direction EW is active it means there is heavy load of cars in EW direction. Unless there is 0 cars in all other directions the direction will not be =EW after 10 clock tick.
property HeavyCarDelayEW;
@(posedge clock)  
(state==EW && sensor == 2'b11  && car_ns!=0 && car_sn!=0 && car_we!=0) |-> ##5 state!=EW;
endproperty


//if sensor==11 and direction SN is active it means there is heavy load of cars in SN direction. Unless there is 0 cars in all other directions the direction will not be =SN after 10 clock tick. 
property HeavyCarDelaySN;
@(posedge clock)  
(state==SN && sensor == 2'b11  && car_ew!=0 && car_ns!=0 && car_we!=0) |-> ##5 state!=SN;
endproperty


//if sensor==11 and direction WE is active it means there is heavy load of cars in WE direction. Unless there is 0 cars in all other directions the direction will not be =WE after 10 clock tick. 
property HeavyCarDelayWE;
@(posedge clock)  
(state==WE && sensor == 2'b11  && car_ew!=0 && car_sn!=0 && car_ns!=0) |-> ##5 state!=WE;
endproperty

//-----------------------------------------------


//If State NS is active and load on the direction is light according to sensor value and the number of cars in that direction is < 4 then within 0 to 5 clock ticks sometime the signal dir_ns will be 10 i.e. yellow.
property LightNSYellow;
@(posedge clock)  
(state==NS && sensor == 2'b01  && car<4) |-> ##[0:3] dir_ns==2'b10;
endproperty



//If State EW is active and load on the direction is light according to sensor value and the number of cars in that direction is < 4 then within 0 to 5 clock ticks sometime the signal dir_ew will be 10 i.e. yellow.
property LightEWYellow;
@(posedge clock)  
(state==EW && sensor == 2'b01  && car<4) |-> ##[0:3] dir_ew==2'b10;
endproperty


//If State SN is active and load on the direction is light according to sensor value and the number of cars in that direction is < 4 then within 0 to 5 clock ticks sometime the signal dir_sn will be 10 i.e. yellow.
property LightSNYellow;
@(posedge clock)  
(state==SN && sensor == 2'b01  && car<4) |-> ##[0:3] dir_sn==2'b10;
endproperty



//If State WE is active and load on the direction is light according to sensor value and the number of cars in that direction is < 4 then within 0 to 5 clock ticks sometime the signal dir_we will be 10 i.e. yellow
property LightWEYellow;
@(posedge clock)  
(state==WE && sensor == 2'b01  && car<4) |-> ##[0:3] dir_we==2'b10;
endproperty


//---------------------------------------


//If State NS is active and load on the direction is heavy according to sensor value and the number of cars in that direction is < 10 then within 0 to 10 clock ticks sometime the signal dir_ns will be 10 i.e. yellow.
property HeavyNSYellow;
@(posedge clock)  
(state==NS && sensor == 2'b11  && car<10) |-> ##[0:5] dir_ns==2'b10;
endproperty


//If State EW is active and load on the direction is heavy according to sensor value and the number of cars in that direction is < 10 then within 0 to 10 clock ticks sometime the signal dir_ew will be 10 i.e. yellow.
property HeavyEWYellow;
@(posedge clock)  
(state==EW && sensor == 2'b11  && car<10) |-> ##[0:5] dir_ew==2'b10;
endproperty


//If State SN is active and load on the direction is heavy according to sensor value and the number of cars in that direction is < 10 then within 0 to 10 clock ticks sometime the signal dir_sn will be 10 i.e. yellow.
property HeavySNYellow;
@(posedge clock)  
(state==SN && sensor == 2'b11  && car<10) |-> ##[0:5] dir_sn==2'b10;
endproperty


//If State WE is active and load on the direction is heavy according to sensor value and the number of cars in that direction is < 10 then within 0 to 10 clock ticks sometime the signal dir_we will be 10 i.e. yellow.
property HeavyWEYellow;
@(posedge clock)  
(state==WE && sensor == 2'b11  && car<10) |-> ##[0:5] dir_we==2'b10;
endproperty

//------------------------------------------------End Properties---------------------------------

always @ (posedge clock, posedge reset)
	begin 
		if (reset == 1'b1) begin
		  state = NS;
			dir=2'b00;
		end
	else begin	 
		case(state)
		
		   NS : 		   
		   if (sensor == 2'b01) 
		   begin
			 #1 ;            
			dir=2'b01;
			state = EW; 
			end
			else if (sensor == 2'b10)
			begin
				      
				dir_ns=2'b11;
				dir_sn=2'b01;
				dir_ew=2'b01;
				dir_we=2'b01;
				car=car_ns;
				car_ns_new = car;
				for(i=0;i<=car-1;i=i+1)
				begin
					#1;
					car_ns_new = car_ns_new - 1;
				end
				count=4-car;
				if(count > 0)
				begin
					dir_ns=2'b10;
					dir_sn=2'b01;
					dir_ew=2'b01;
					dir_we=2'b01;
					for(i=0;i<count;i=i+1)
					begin
						#1;
					//car_ns_new = car_ns_new - 1;
					end
				end
				dir=2'b01;
				state= EW;
				
			end
			else if (sensor == 2'b11)
			begin
				dir_ns=2'b11;
				dir_sn=2'b01;
				dir_ew=2'b01;
				dir_we=2'b01;
				car=car_ns;
				if(car <=10)
				begin
		
					car_ns_new = car;
					for(i=0;i<=car-1;i=i+1)
					begin
						#1;
						car_ns_new = car_ns_new - 1;
					end
					count=10-car;
					if(count > 0) begin
					dir_ns=2'b10;
					dir_sn=2'b01;
					dir_ew=2'b01;
					dir_we=2'b01;
					for(i=0;i<count;i=i+1)
				begin
					#1;
					
				end
				end
				end
			
				else if(car>10)
				begin
					car_ns_new = car;
					for(i=0; i < 10; i=i+1)
					begin
						#1;
						car_ns_new = car_ns_new - 1;
					end
				end
				dir=2'b01;
				state = EW;
		
				
				end 
		
		   EW : if (sensor == 2'b01) begin
					 #1 ;
				        state = SN;
					dir=2'b10;
				      end else if (sensor == 2'b10) begin
				dir_ns=2'b01;
				dir_sn=2'b01;
				dir_ew=2'b11;
				dir_we=2'b01;
				car=car_ew;
				car_ew_new = car;
				for(i=0;i<=car-1;i=i+1)
				begin
					#1;
					car_ew_new = car_ew_new - 1;
				end
	
				count=4-car;
				if(count > 0) begin
				dir_ns=2'b01;
				dir_sn=2'b01;
				dir_ew=2'b10;
				dir_we=2'b01;
				for(i=0;i<count;i=i+1)
				begin
					#1;
					
				end
				end
				state= SN;
		
				dir=2'b10;
				      end else if (sensor == 2'b11) begin
				
				dir_ns=2'b01;
				dir_sn=2'b01;
				dir_ew=2'b11;
				dir_we=2'b01;
				car=car_ew;
		
				if(car<=10)
				begin
				car_ew_new = car;
				for(i=0;i<=car-1;i=i+1)
				begin
					#1;
					car_ew_new = car_ew_new - 1;
				end
					count=10-car;
					if(count > 0) begin
					dir_ns=2'b01;
					dir_sn=2'b01;
					dir_ew=2'b10;
					dir_we=2'b01;
					for(i=0;i<count;i=i+1)
				begin
					#1;
					
				end
				end
				end
		
				else if(car>10)
				begin
					car_ew_new = car;
					for(i = 0; i < 10; i=i+1)
					begin
						#1;
						car_ew_new = car_ew_new-1;
					end
				end
				state = SN;
		
				dir=2'b10;
				      end 
		   SN : if (sensor == 2'b01)
		   	begin
		   	 	#1 ;
				state = WE;
				dir=2'b11;
			end
			else if (sensor == 2'b10)
			begin
				dir_ns=2'b01;
				dir_sn=2'b11;
				dir_ew=2'b01;
				dir_we=2'b01;
				car=car_sn;
		
				car_sn_new = car;
				for(i=0;i<=car-1;i=i+1)
				begin
					#1;
					car_sn_new = car_sn_new - 1;
				end
				count=4-car;
				if(count > 0)
				begin
					dir_ns=2'b01;
					dir_sn=2'b10;
					dir_ew=2'b01;
					dir_we=2'b01;
					for(i=0;i<count;i=i+1)
					begin
						#1;
					
					end
				end
				state= WE;
		
				dir=2'b11;
			end
			else if (sensor == 2'b11)
			begin
				dir_ns=2'b01;
				dir_sn=2'b11;
				dir_ew=2'b01;
				dir_we=2'b01;
				car=car_sn;
		
				if(car <= 10)
				begin
					car_sn_new = car;
					for(i=0;i<=car-1;i=i+1)
					begin
						#1;
						car_sn_new = car_sn_new - 1;
					end
					count=10-car;
					if(count > 0)
					begin
						dir_ns=2'b01;
						dir_sn=2'b10;
						dir_ew=2'b01;
						dir_we=2'b01;
						for(i=0;i<count;i=i+1)
						begin
							#1;
							//car_ns_new = car_ns_new - 1;
						end
					end
				end
		
				else if(car>10)
				begin
					car_sn_new = car;
					for(i = 0; i < 10; i = i+1)
					begin
						#1;
						car_sn_new = car_sn_new - 1;
					end
				end
				state = WE;
		
				dir=2'b11;
			end 
		  WE : if (sensor == 2'b01) 
		  	begin
					 
				#1 ;
				dir=2'b00;
				state = NS;
			end
			else if (sensor == 2'b10)
			begin
				dir_ns=2'b01;
				dir_sn=2'b01;
				dir_ew=2'b01;
				dir_we=2'b11;
				car=car_we;
		
				car_we_new = car;
				for(i=0;i<=car-1;i=i+1)
				begin
					#1;
					car_we_new = car_we_new - 1;
				end
				count=4-car;
				if(count > 0)
				begin
					dir_ns=2'b01;
					dir_sn=2'b01;
					dir_ew=2'b01;
					dir_we=2'b10;
					for(i=0;i<count;i=i+1)
					begin
						#1;
					end
				end
				dir=2'b00;
				state= NS;
		
				end
				else if (sensor == 2'b11)
				begin
					dir_ns=2'b01;
					dir_sn=2'b01;
					dir_ew=2'b01;
					dir_we=2'b11;
					car=car_we;
		
					if(car <= 10)
					begin
						car_we_new = car;
						for(i=0;i<=car-1;i=i+1)
						begin
							#1;
							car_we_new = car_we_new - 1;
						end
						count=10-car;
						if(count > 0)
						begin
							dir_ns=2'b01;
							dir_sn=2'b01;
							dir_ew=2'b01;
							dir_we=2'b10;
							for(i=0;i<count;i=i+1)
							begin
								#1;
							end
						end
					end
		
		
					else if(car>10)
					begin
						car_we_new = car;
						for(i = 0; i <10; i=i+1)
						begin
							#1;
							car_we_new = car_we_new-1;
						end
					end
					dir=2'b00;
					state = NS;
		
				
				end 
		endcase
	end
end


//----------------------------------System Verilog Assertions----------------------------
assert property(NorthSouthSignal) else $error("NorthSouthSignal incorrect");
assert property(EastWestSignal) else $error("EastWestSignal incorrect");
assert property(SouthNorthSignal) else $error("SouthNorthSignal incorrect");
assert property(WestEastSignal) else $error("WestEastSignal incorrect");

assert property(StateActiveNS) else $error("StateActiveNS incorrect");
assert property(StateActiveEW) else $error("StateActiveEW incorrect");
assert property(StateActiveSN) else $error("StateActiveSN incorrect");
assert property(StateActiveWE) else $error("StateActiveWE incorrect");


assert property(ZeroCarDelayNS) else $error("ZeroCarDelayNS incorrect");
assert property(ZeroCarDelayEW) else $error("ZeroCarDelayEW incorrect");
assert property(ZeroCarDelaySN) else $error("ZeroCarDelaySN incorrect");
assert property(ZeroCarDelayWE) else $error("ZeroCarDelayWE incorrect");


assert property(LightCarDelayNS) else $error("LightCarDelayNS incorrect");
assert property(LightCarDelayEW) else $error("LightCarDelayEW incorrect");
assert property(LightCarDelaySN) else $error("LightCarDelaySN incorrect");
assert property(LightCarDelayWE) else $error("LightCarDelayWE incorrect");

assert property(HeavyCarDelayNS) else $error("HeavyCarDelayNS incorrect");
assert property(HeavyCarDelayEW) else $error("HeavyCarDelayEW incorrect");
assert property(HeavyCarDelaySN) else $error("HeavyCarDelaySN incorrect");
assert property(HeavyCarDelayWE) else $error("HeavyCarDelayWE incorrect");

assert property(LightNSYellow) else $error("LightNSYellow incorrect");
assert property(LightEWYellow) else $error("LightEWYellow incorrect");
assert property(LightSNYellow) else $error("LightSNYellow incorrect");
assert property(LightWEYellow) else $error("LightWEYellow incorrect");

assert property(HeavyNSYellow) else $error("HeavyNSYellow incorrect");
assert property(HeavyEWYellow) else $error("HeavyEWYellow incorrect");
assert property(HeavySNYellow) else $error("HeavySNYellow incorrect");
assert property(HeavyWEYellow) else $error("HeavyWEYellow incorrect");
//----------------------------------------------End Assertions----------------------

endmodule


/*
Module: controller
Input: clock, reset
Ouput: dir_ns, dir_ew, dir_sn, dir_we, car_ns, car_ew, car_sn, car_we
Purpose: ties together car, sensor and direction modules 
*/
module controller
(
clock,
reset,
dir_ns,dir_sn,dir_ew,dir_we ,car_ns_new,car_sn_new,car_ew_new,car_we_new,
);

input clock,reset;

output[1:0] dir_ns,dir_sn,dir_ew,dir_we;
output[3:0] car_ns_new,car_sn_new,car_ew_new,car_we_new;

wire[3:0] car_ns,car_sn,car_ew,car_we ;
wire[3:0] car_ns_new,car_sn_new,car_ew_new,car_we_new ;
wire[1:0] sensor;
wire[1:0] dir;

car r(clock,reset,car_ns,car_sn,car_ew,car_we,car_ns_new,car_ew_new,car_sn_new,car_we_new,dir_ns, dir_ew, dir_we,dir_sn);
sensor s(clock,reset,car_ns,car_sn,car_ew,car_we,dir,sensor );
direction d(clock,reset,sensor,car_ns,car_sn,car_ew,car_we,dir_ns,dir_sn,dir_ew,dir_we,dir,car_ns_new,car_ew_new,car_sn_new,car_we_new);

endmodule
